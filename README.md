# libreML

This allows you to access the libreML docker container using the supplied python script.

This has options for either accessing a jupyter notebook instance, or as an interactive shell.

To get a jupyter notebook instance simply go to the working directory you want and type

```shell
libreml
```

To access an interactive shell in the current working directory
```shell
libreml --mode=shell
```

For further instructions, `libreml --help` will provide more details.
